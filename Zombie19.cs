﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zombie_Edgar_Buchs_S9ILAG
{
    public class Zombie19
    {
        private Dictionary<Group, bool> shouldInfectGroupCache = new Dictionary<Group, bool>();


        /*-----------------------------------Zombie 19--------------------------------------------*/
        public Group SpreadInfection(Group group)
        {
            if (CheckIfGroupOrSubgroupsInfected(group))
                return InfectAllMembers(group);
         return group;
        }

        //Zombie 19
        private bool CheckIfGroupOrSubgroupsInfected(Group group)
        {
            return group.Members.Any(member =>
                (member is Person person && person.IsInfected) ||
                (member is Group subGroup && CheckIfGroupOrSubgroupsInfected(subGroup)));
        }

        //Zombie 19
        private Group InfectAllMembers(Group group)
        {
            return new Group(group.Members.Select(member =>
                member switch
                {
                    Person person => new Person(person.Name, person.Age, true, true),
                    Group subGroup => InfectAllMembers(subGroup),
                    _ => member
                }).ToList());
        }
        /*-----------------------------------Zombie 19--------------------------------------------*/
        //
        /*-----------------------------------Zombie A--------------------------------------------*/
        public Group SpreadInfectionA(Group group)
        {
        var isTopLevelGroupInfected = group.Members.OfType<Person>().Any(p => p.IsInfected);

            if (isTopLevelGroupInfected)
                return InfectAllMembers(group);
        return new Group(group.Members.Select(
                                        member => member switch
                                                                {
                                                                    Person person => person,
                                                                    Group subGroup => SpreadInfectionA(subGroup),
                                                                    _ => member
                                                                }).ToList());
        }
        /*Ordre sup*/
        public Group SpreadInfectionA(Group group, Func<Person, bool, Person> infectionStrategy)
        {
            var isTopLevelGroupInfected = group.Members.OfType<Person>().Any(p => p.IsInfected);

            if (isTopLevelGroupInfected)
                return new Group(group.Members.Select(member => ApplyInfectionStrategy(member, infectionStrategy, true)).ToList());

            return new Group(group.Members.Select(member => ApplyInfectionStrategy(member, infectionStrategy, false)).ToList());
        }

        private object ApplyInfectionStrategy(object member, Func<Person, bool, Person> infectionStrategy, bool shouldInfect)
        {
            return member switch
            {
                Person person when shouldInfect => infectionStrategy(person, shouldInfect),
                Group subGroup => SpreadInfectionA(subGroup, infectionStrategy),
                _ => member
            };
        }

        public Person DefaultInfectionStrategy(Person person, bool shouldInfect)
        {
            return shouldInfect ? new Person(person.Name, person.Age, true, true) : person;
        }
        /*-----------------------------------Zombie A--------------------------------------------*/
        //
        /*-----------------------------------Zombie B--------------------------------------------*/
        public Group SpreadInfectionB(Group group)
        {
            var maybeShouldInfectParent = ShouldInfectParent(group);

            var updatedMembers = group.Members.Select(member =>
            {
                if (member is Group subGroup)
                {
                    var subInfectedB = SpreadInfectionB(subGroup);
                    return subInfectedB;
                }
                else if (member is Person person && maybeShouldInfectParent.HasValue && maybeShouldInfectParent.Value)
                {
                    return new Person(person.Name, person.Age, true, true);
                }
                else
                {
                    return member;
                }
            }).ToList();

            return new Group(updatedMembers);
        }

        //Zombie B
        public Maybe<bool> ShouldInfectParent(Group group)
        {
            foreach (var member in group.Members)
            {
                if (member is Person person && person.IsInfected)
                {
                    return Maybe<bool>.Some(true);
                }
                else if (member is Group subGroup)
                {
                    var result = ShouldInfectParent(subGroup);
                    if (result.HasValue && result.Value)
                    {
                        return Maybe<bool>.Some(true);
                    }
                }
            }

            return Maybe<bool>.None();
        }

        /*-----------------------------------Zombie B--------------------------------------------*/
        //
        /*-----------------------------------Zombie C--------------------------------------------*/
        public Group SpreadInfectionC(Group group)
        {
            Group infectedGroup = InfectGroup(group);
            return infectedGroup;
        }

        //Zombie C
        private Group InfectGroup(Group group)
        {
            var newMembers = group.Members.Select(member =>
            {
                if (member is Group subgroup)
                {
                    return InfectGroup(subgroup);
                }
                return member;
            }).ToList();

            var newGroup = new Group(newMembers);

            if (ShouldInfectGroup(newGroup))
            {
                var infectedSubGroup = InfectSubgroup(newGroup);
                return infectedSubGroup;
            }
            else
            {
                return newGroup;
            }
        }

        //Zombie C
        private bool ShouldInfectGroup(Group group)
        {
            if (shouldInfectGroupCache.TryGetValue(group, out bool cachedResult))
            {
                return cachedResult;
            }

            int personCount = 0;
            bool hasInfectedPerson = false;

            foreach (var member in group.Members)
            {
                if (member is Person person)
                {
                    personCount++;
                    if (person.IsInfected)
                    {
                        hasInfectedPerson = true;
                    }
                }
            }

            bool result = personCount > 2 && hasInfectedPerson;

            shouldInfectGroupCache[group] = result;
            return result;
        }

        //Zombie C
        private Group InfectSubgroup(Group subgroup)
        {
            var persons = subgroup.Members.OfType<Person>().ToList();
            int firstInfectedIndex = persons.FindIndex(p => p.IsInfected);

            if (firstInfectedIndex != -1)
            {
                bool isFirstInfectedIndexEven = firstInfectedIndex % 2 == 0;
                int startIndex = isFirstInfectedIndexEven ? 0 : 1;

                var newPersons = persons.Select((person, index) =>
                {
                    if (index >= startIndex && (index - startIndex) % 2 == 0 && !person.IsInfected)
                    {
                        return person with { IsInfected = true };
                    }
                    return person;
                }).ToList();

                var newMembers = subgroup.Members.Select(member =>
                {
                    if (member is Person person)
                    {
                        return newPersons.Find(p => p.Name == person.Name && p.Age == person.Age);
                    }
                    return member;
                }).ToList();

                return new Group(newMembers);
            }
            else
            {
                return subgroup;
            }
        }
        /*-----------------------------------Zombie C--------------------------------------------*/
        //
        /*-----------------------------------Zombie 32--------------------------------------------*/
        public Group SpreadInfection32(Group group)
        {
            return SpreadInfection32Helper(group, false);
        }

        //Zombie 32
        private Group SpreadInfection32Helper(Group group, bool shouldInfect)
        {
            bool isInfected32Present = CheckIfGroupOrSubgroupsInfected32(group);

            var newMembers = group.Members.Select(member =>
            {
                switch (member)
                {
                    case Group subGroup:
                        return SpreadInfection32Helper(subGroup, isInfected32Present || shouldInfect);
                    case Person person when shouldInfect && person.Age >= 32:
                        return new Person(person.Name, person.Age, true, true);
                    default:
                        return member;
                }
            }).ToList();

            return new Group(newMembers);
        }

        //Zombie 32
        private bool CheckIfGroupOrSubgroupsInfected32(Group group)
        {
            return group.Members.Any(member =>
                (member is Person person && person.IsInfected && person.Age >= 32) ||
                (member is Group subGroup && CheckIfGroupOrSubgroupsInfected32(subGroup)));
        }

        /*-----------------------------------Zombie 32--------------------------------------------*/
        //
        /*-----------------------------------Zombie Ultime--------------------------------------------*/
        public Group SpreadInfectionUltime(Group group)
        {
            return InfectRootPerson(group);
        }
        //Zombie Ultime
        private Group InfectRootPerson(Group group, bool hasInfected = false)
        {
            var updatedMembers = new List<object>();
            bool infected = hasInfected;

            foreach (var member in group.Members)
            {
                if (member is Person person && !infected)
                {
                    updatedMembers.Add(new Person(person.Name, person.Age, true, true));
                    infected = true;
                }
                else if (member is Group subGroup)
                {
                    updatedMembers.Add(InfectRootPerson(subGroup, infected)); 
                }
                else
                {
                    updatedMembers.Add(member);
                }
            }

            return new Group(updatedMembers);
        }

        /*-----------------------------------Zombie Ultime--------------------------------------------*/
    }
}
