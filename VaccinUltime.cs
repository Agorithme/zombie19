﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zombie_Edgar_Buchs_S9ILAG
{
    public class VaccinUltime
    {
        public Group DeployVaccine(Group infectedGroup)
        {
            return ApplyVaccineUltime(infectedGroup);
        }

        private Group ApplyVaccineUltime(Group group)
        {
            var newMembers = group.Members.Select(member =>
            {
                switch (member)
                {
                    case Person person when person.IsInfected:
                        return new Person(person.Name, person.Age, true, false, true, true);

                    case Group subGroup:
                        return ApplyVaccineUltime(subGroup);

                    default:
                        return member;
                }
            }).ToList();

            return new Group(newMembers);
        }

    }
}
