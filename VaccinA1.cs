﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zombie_Edgar_Buchs_S9ILAG
{
    public class VaccinA1
    {
        public Group DeployVaccine(Group infectedGroup)
        {
            return ApplyVaccineA1(infectedGroup);
        }

        private Group ApplyVaccineA1(Group group)
        {
            var newMembers = group.Members.Select(member =>
            {
                if (member is Person person)
                {
                    if (person.Age <= 30 && person.IsInfected)
                    {
                        return new Person(person.Name, person.Age, true, false);
                    }
                    return person;
                }
                else if (member is Group subGroup)
                {
                    return ApplyVaccineA1(subGroup);
                }
                else
                {
                    return member;
                }
            }).ToList();

            return new Group(newMembers);
        }
    }
    
}
