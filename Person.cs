﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zombie_Edgar_Buchs_S9ILAG
{
    public record Person(string Name, int Age,bool IsAlive = true, bool IsInfected = false, bool IsImmune = false, bool CannotInfectOthers = false);
}
