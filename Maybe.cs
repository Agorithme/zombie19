﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zombie_Edgar_Buchs_S9ILAG
{
    public class Maybe<T>
    {
        public T Value { get; }
        public bool HasValue { get; }

        private Maybe(T value, bool hasValue)
        {
            Value = value;
            HasValue = hasValue;
        }

        public static Maybe<T> Some(T value) => new Maybe<T>(value, true);
        public static Maybe<T> None() => new Maybe<T>(default(T), false);
    }

}
