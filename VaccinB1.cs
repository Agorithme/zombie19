﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zombie_Edgar_Buchs_S9ILAG
{
    public class VaccinB1
    {
        public Group DeployVaccine(Group infectedGroup)
        {
            return ApplyVaccineB1(infectedGroup);
        }

        private Group ApplyVaccineB1(Group group)
        {
            var newMembers = group.Members.Select((member, index) =>
            {
                if (member is Person person && person.IsInfected)
                {
                    if (index % 2 == 0)
                    {
                        return new Person(person.Name, person.Age, true, false);
                    }
                    else
                    {
                        return new Person(person.Name, person.Age, false, false);
                    }
                }
                else if (member is Group subGroup)
                {
                    return ApplyVaccineB1(subGroup);
                }
                else
                {
                    return member;
                }
            }).ToList();

            return new Group(newMembers.Where(m => m != null).ToList());
        }
    }
}
