﻿namespace Zombie_Edgar_Buchs_S9ILAG
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var group = 
            new Group(new List<object>
            {
                new Person("God", 1000, true, false),
                new Group(new List<object>
                {
                new Person("Edgar", 26, true, false),
                new Group(new List<object>
                {
                    new Person("Patrick", 34, true, false),
                    new Person("Dodo", 30, true, false),
                    new Person("Mickey", 20, true, true),
                    new Group(new List<object>
                    {
                        new Person("Charlie", 35, true, false),
                        new Person("Pika", 28, true, false),
                        new Person("Panpan", 38, true, false),
                        new Person("Roudolf", 50, true, true),
                    })
                }),
                new Person("Jean-Matthieu", 20, true, false)                    
                }),
            });


            //Console.WriteLine("//////////////////////////////////////////////////////////////");
            Console.WriteLine("-------------------------Zombie A-----------------------------");
            var newZombieInfA = new Zombie19();
            var infectedGroupSuppA = newZombieInfA.SpreadInfectionA(group, newZombieInfA.DefaultInfectionStrategy);
            PrintGroup(infectedGroupSuppA, 0);
            var infectedGroupA = newZombieInfA.SpreadInfectionA(group);
            PrintGroup(infectedGroupA, 0);
            //Console.WriteLine("-------------------------Zombie 32-----------------------------");
            //var newZombieInf32 = new Zombie19();
            //var infectedGroup32 = newZombieInf32.SpreadInfection32(group);
            //PrintGroup(infectedGroup32, 0);
            //Console.WriteLine("-------------------------Vaccin A1-----------------------------");
            ////Utilisation du vaccin A1
            //var newVaccinA1 = new VaccinA1();
            //var vaccineGroupA1 = newVaccinA1.DeployVaccine(group);
            //PrintGroup(vaccineGroupA1, 0);
            //Console.WriteLine("//////////////////////////////////////////////////////////////");
            //Console.WriteLine("-------------------------Zombie B-----------------------------");
            //var newZombieInfB = new Zombie19();
            //var infectedGroupB = newZombieInfB.SpreadInfectionB(group);
            //PrintGroup(infectedGroupB, 0);
            //Console.WriteLine("-------------------------Zombie C-----------------------------");
            //var newZombieInfC = new Zombie19();
            //var infectedGroupC = newZombieInfC.SpreadInfectionC(group);
            //PrintGroup(infectedGroupC, 0);
            //Console.WriteLine("-------------------------Vaccin B1-----------------------------");
            ////Utilisation du vaccin B1
            //var newVaccinB1 = new VaccinB1();
            //var vaccineGroupB1 = newVaccinB1.DeployVaccine(group);
            //PrintGroup(vaccineGroupB1, 0);
            //Console.WriteLine("//////////////////////////////////////////////////////////////");
            //Console.WriteLine("-------------------------Zombie Ultime-----------------------------");
            //var newZombieInfUltime = new Zombie19();
            //var infectedGroupUltime = newZombieInfUltime.SpreadInfectionUltime(group);
            //PrintGroup(infectedGroupUltime, 0);
            //Console.WriteLine("-------------------------Vaccin Ultime-----------------------------");
            ////Utilisation du vaccin ultime
            //var newVaccinUltime = new VaccinUltime();
            //var vaccineGroupUltime = newVaccinUltime.DeployVaccine(group);
            //PrintGroup(vaccineGroupUltime, 0);



        }

        static void PrintGroup(Group group, int level)
        {
            foreach (var member in group.Members)
            {
                if (member is Person person)
                {
                    Console.WriteLine(new String('-', level * 2) + $"Person: {person.Name}, " +
                                                                   $"Age: {person.Age}, " +
                                                                   $"IsAlive: {person.IsAlive}, " +
                                                                   $"Infected: {person.IsInfected}, " +
                                                                   $"IsImmune: {person.IsImmune}, " +
                                                                   $"CannotInfectOthers: {person.CannotInfectOthers}");
                }
                else if (member is Group subgroup)
                {
                    Console.WriteLine(new String('-', level * 2) + "Subgroup:");
                    PrintGroup(subgroup, level + 1);
                }
            }
        }
    }
}